# mandoc

UNIX manpage compiler toolset. http://mandoc.bsd.lv/

## Possible compatibility issue
* AUR mandoc provides man and not mman like in other distributions 
  like Debian, this may make an incompatibility with the main man
  package of the distribution.

## Projects using this one
* [ocaml](https://gitlab.com/pacman-packages-demo/ocaml)
* [smlnj](https://gitlab.com/pacman-packages-demo/smlnj)

## This project compiled package from AUR and man page(s) are at:
* [pacman-packages-demo.gitlab.io/mandoc
  ](https://pacman-packages-demo.gitlab.io/mandoc)